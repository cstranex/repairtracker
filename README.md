# Repair Tracker

This was an internal project for tracking repair jobs in the same vein as an issue tracker.

It lacks many features of a full issue tracker and was mostly being built as necessary for the needs of the team.

## Installation

Use the supplied dockerfile to create your docker image. It also requires a database (designed with MySQL in mind but
should work with PostgreSQL).

```yaml
# Example of a possible docker-compose.yml

version: '2'

services:
    tracker:
      image: tracker:latest
      restart: always
      environment:
        - SECRET_KEY=somesecretkey
        - DATABASE_URI=mysql://issues:issues@database/issues
        - HOSTED_DOMAIN=domain.com
        - GOOGLE_OAUTH_CLIENT_SECRET=xyz
        - GOOGLE_OAUTH_CLIENT_ID=abc
     volumes:
       - ./gunicorn.conf:/etc/gunicorn.conf:ro
    database:
        image: mysql:latest
        environment:
           - MYSQL_DATABASE=issues
```

After starting you will need to create the database for the tracker before using by running `flask init`. You can also
create categories, tags and severities by using `flask create-[category|tag|severity] NAME`

## Configuration

Configuration is done with environment variables. See `config/common.py` and `config/prod.py` for the various
configuration variables.

Authentication is handled via Google Authentication so you will need to obtain an OAuth Client
Secret and ID from the Google Developer's Console. The webapp was also designed with hosted domains in mind.   
