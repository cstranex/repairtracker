import os
from typing import Type
from .common import Config


def get_configuration() -> Type[Config]:
    """Returns the active configuration depending on the FLASK_ENV environment variable"""

    env = os.getenv('FLASK_ENV', 'production')
    if env == 'development':
        from .dev import Config
    elif env == 'production':
        from .prod import Config
    return Config
