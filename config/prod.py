import os
from . import common


class Config(common.Config):
    SECRET_KEY = os.getenv('SECRET_KEY')

