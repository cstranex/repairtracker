import os
from . import common


class Config(common.Config):
    SECRET_KEY = 'iamsecret'


os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
