from flask import Blueprint, render_template, flash

from model import db
from admin.forms import TagForm
from auth.authentication import login_required, current_user
from tags.models import Tag

app = Blueprint('admin', __name__, url_prefix='/admin')


@app.before_request
@login_required
def before_request():
    """Ensure all endpoints are authenticated"""
    if not current_user.admin:
        return "Not authorised", 403


@app.route('/', methods=['GET', 'POST'])
def view():
    form = TagForm()
    if form.validate_on_submit():
        t = Tag()
        t.name = form.name.data
        t.enabled = True
        db.session.add(t)
        db.session.commit()
        flash("Created tag", 'success')

    return render_template('admin/index.html', form=form)
