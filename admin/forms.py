from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired, Length


class TagForm(FlaskForm):
    name = StringField(label="Name", validators=[DataRequired(), Length(min=1, max=100)])
