from typing import Optional

import markupsafe

from issue.models import Issue


def referenceissue(reference: str) -> Optional[Issue]:
    try:
        return Issue.query.get(int(reference))
    except ValueError:
        return None


def renderissue(issue: Issue, hide_badge=False):
    if not issue:
        return '<Invalid Issue>'

    text = markupsafe.escape(issue.formatted_title)

    if not issue.is_open:
        text = f'<span class="issue closed">{text}</span>'
    else:
        text = f'<span class="issue open">{text}</span>'

    if hide_badge:
        return text

    return text + f' <span class="ui {issue.status.colour_name} label">{markupsafe.escape(issue.status.name)}</span>'
