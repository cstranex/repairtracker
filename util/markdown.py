import re
import markdown2
from markupsafe import Markup

from util.qrcode import QR_RE

link_patterns = [
    (re.compile(r'((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+(:[0-9]+)?|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)'),r'\1'),
    (re.compile("#([0-9]{3}[0-9]+)"), r"/issue/\1"),
    (re.compile("#([a-zA-Z0-9]+)"), r"/tag/name/\1"),
    (QR_RE, r"https://waretrace.com/qr/\2"),
]


def markdown(text):
    return Markup(
        markdown2.markdown(
            text,
            extras=["link-patterns", "target-blank-links", "tag-friendly"],
            link_patterns=link_patterns
        )
    )
