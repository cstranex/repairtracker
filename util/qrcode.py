import re

QR_RE = re.compile(r'(HTTP://WARETRACE\.COM/QR/)?([BCDFGHJKLMNPQRSTVWXYZ]{14}[0-9]{2})', re.IGNORECASE)


def validate_qr_code(code: str) -> bool:
    if QR_RE.match(code):
        # Check digit
        digits = ''
        for c in code[:-2]:
            digits += str(ord(c) - ord('A') + 10)

        check = code[-2:]
        digits += check

        return int(digits) % 97 == 1
