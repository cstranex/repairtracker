from wtforms import ValidationError, FieldList
from wtforms_alchemy import model_form_factory, QuerySelectMultipleField, ModelFieldList
from flask_wtf import FlaskForm
from wtforms_alchemy.utils import find_entity

from model import db


BaseModelForm = model_form_factory(FlaskForm)


class ModelForm(BaseModelForm):
    @classmethod
    def get_session(self):
        return db.session


class AppendModelFieldList(ModelFieldList):

    def populate_obj(self, obj, name):
        state = obj._sa_instance_state

        if not state.identity or self.population_strategy == 'replace':
            super(AppendModelFieldList, self).populate_obj(obj, name)
        else:
            _fake = type(str('_fake'), (object,), {})
            coll = getattr(obj, name)
            entities = coll
            for index, entry in enumerate(self.entries):
                data = entry.data
                entity = find_entity(coll, self.model, data)

                if entity is None:
                    fake_obj = _fake()
                    fake_obj.data = self.model()
                    entry.populate_obj(fake_obj, 'data')
                    entities.append(fake_obj.data)

            setattr(obj, name, entities)


class AutoDeleteModelField(ModelFieldList):
    def populate_obj(self, obj, name):
        coll = getattr(obj, name)

        entities = []

        for index, entry in enumerate(self.entries):
            data = entry.data
            entity = find_entity(coll, self.model, data)
            if entity is None:
                entities.insert(index, self.model())
            else:
                entities.append(entity)
                coll.remove(entity)

        for item in coll:
            # Delete these items
            db.session.delete(item)

        setattr(obj, name, entities)
        FieldList.populate_obj(self, obj, name)


class AutoCreateQueryMultipleSelectField(QuerySelectMultipleField):

    def __init__(self, label=None, validators=None, default=None, create_factory=None, **kwargs):
        self._create_factory = create_factory
        super(AutoCreateQueryMultipleSelectField, self).__init__(label, validators, default, **kwargs)

    def pre_validate(self, form):
        if self._invalid_formdata:
            raise ValidationError(self.gettext('Not a valid choice'))
        return

    def populate_obj(self, obj, name):
        # At this point, we create the new item
        if self._formdata is None:
            return

        data = []

        for pk, item in self._get_object_list():
            if pk in self._formdata:
                data.append(item)
                self._formdata.remove(pk)

        for key in self._formdata:
            item = self._create_factory(key)
            db.session.add(item)
            data.append(item)

        self.data = data

        setattr(obj, name, data)
