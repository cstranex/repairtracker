from datetime import datetime

from flask_babelex import format_datetime as babel_format_datetime


def format_datetime(value, format='medium'):
    if value is None:
        return ''

    if format == 'timeago':
        now = datetime.utcnow()
        difference = (now - value)
        if difference.days == 0:
            date = "'Today'"
        elif difference.days == 1:
            date = "'Yesterday'"
        else:
            date = 'E d MMM yyyy'

        time = " 'at' kk:mm"
        format = date + time

    return babel_format_datetime(value, format)
