from functools import lru_cache

import requests

import jwt
from flask import current_app


def verify_id_token():
    jwks = get_discovery_document()['jwks_uri']

    token = jwt.decode(r.json()['id_token'], verify=True)

    # Ensure the realm is the same
    if token['hd'] != current_app.config.get('hosted_domain'):
        return False


@lru_cache()
def get_jwks(url):
    r = requests.get('https://accounts.google.com/.well-known/openid-configuration')
    if not r.ok:
        raise RuntimeError("Discovery Document failed")
    return r.json()['keys']


@lru_cache()
def get_discovery_document():
    r = requests.get('https://accounts.google.com/.well-known/openid-configuration')
    if not r.ok:
        raise RuntimeError("Discovery Document failed")
    return r.json()
