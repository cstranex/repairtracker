from functools import wraps, partial

from flask.globals import _lookup_app_object
from jwt import JWT

from flask import redirect, url_for

from user.models import User

try:
    from flask import _app_ctx_stack as stack
except ImportError:
    from flask import _request_ctx_stack as stack

from flask_dance.contrib.google import make_google_blueprint, google
from werkzeug.local import LocalProxy

"""
    Users are authenticated using Google OpenId/OAuth2
    Refresh and access tokens are obtained and stored in the session
"""


def create_blueprint(app):
    blueprint = make_google_blueprint(
        scope=['openid', 'https://www.googleapis.com/auth/userinfo.email'],
        hosted_domain=app.config['HOSTED_DOMAIN'],
        redirect_to='google.verify'
    )
    blueprint.authorization_url_params['openid.realm'] = app.config.get('HOSTED_DOMAIN')

    @blueprint.route('/verify')
    def verify():
        """Verify the id token allows the hd param"""
        if google.authorized:
            result = decode_jwt()
            if result.get('hd') != app.config.get('HOSTED_DOMAIN'):
                google.teardown_session()
                return "Domain not allowed", 401

            # Create the user if they don't already exist
            if not User.get_user_by_email(result['email']):
                User.create_user(result['email'], result['email'])

        return redirect('/')

    app.register_blueprint(blueprint, prefix='/login')


def decode_jwt():
    return JWT().decode(message=google.token.get('id_token'), do_verify=False)


def login_required(f):
    """Ensure user is logged in"""

    @wraps(f)
    def login_check(*args, **kwargs):
        if not google.authorized:
            return redirect(url_for('google.login'))
        else:
            result = decode_jwt()
            ctx = stack.top
            ctx.current_user = User.get_user_by_email(result['email'])
            if not ctx.current_user or ctx.current_user.disabled:
                # google.teardown_session()
                return "Account unauthorised", 403

            return f(*args, **kwargs)

    return login_check


current_user = LocalProxy(partial(_lookup_app_object, 'current_user'))
