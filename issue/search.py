import re

from issue.models import ReferenceType
from tags.models import Tag
from .models import Issue, IssueReference

ISSUE_RE = re.compile(r'#([0-9]+)')
TAG_RE = re.compile(r'#([a-zA-Z0-9]+)')


def search_issues(query: str):
    results = []

    match = ISSUE_RE.match(query)
    if match:
        issue = Issue.query.get(int(match.group(1)))
        if issue:
            yield {
                'id': issue.id,
                'title': issue.formatted_title
            }
            return

    match = TAG_RE.match(query)
    if match:
        tag = Tag.query.filter(Tag.name.ilike(match.group(1))).first()
        if tag:
            results += Issue.query.filter(Issue.tags.contains_(tag))

    for result in IssueReference.query.filter(
            IssueReference.value.ilike('%' + query + '%'),
            IssueReference.reference_type != ReferenceType.ISSUE
    ):
        results.append(result.issue)

    results += Issue.query.filter(Issue.title.ilike(f"%{query}%"))

    for result in results:
        yield {
            'id': result.id,
            'title': result.formatted_title
        }

