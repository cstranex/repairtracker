from flask import Blueprint, render_template, redirect, url_for, flash, request

from auth.authentication import login_required, current_user
from issue import models
from issue.forms import IssueForm
from model import db

app = Blueprint('issue', __name__, url_prefix='/issue')


@app.before_request
@login_required
def before_request():
    """Ensure all endpoints are authenticated"""


@app.route('/')
def overview():
    """View all issues"""
    issues = models.Issue.query.order_by(models.Issue.updated.desc())

    return render_template('issue/overview.html', issues=issues)


@app.route('/new', methods=['GET', 'POST'])
def create():
    form = IssueForm()

    if form.validate_on_submit():
        db.session.autoflush = False
        issue = models.Issue()
        form.populate_obj(issue)
        db.session.add(issue)
        db.session.commit()
        return redirect(url_for('issue.view', issue=issue.id))

    return render_template('issue/new.html', form=form)


@app.route('/<int:issue>', methods=['GET', 'POST'])
def view(issue: int):
    """View a specific issue"""

    issue = db.session.query(models.Issue).get_or_404(issue)

    form = IssueForm(obj=issue)
    if form.validate_on_submit():
        db.session.autoflush = False
        form.populate_obj(issue)
        db.session.add(issue)
        db.session.commit()

    return render_template('issue/view.html', issue=issue, statuses=models.Status.get_ordered(), form=form)


@app.route('/search')
def search():
    return 'Not Implemented'
