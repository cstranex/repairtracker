import wtforms
from wtforms import ValidationError
from wtforms_alchemy import QuerySelectField

from tags.models import Tag
from util.form import ModelForm, AutoCreateQueryMultipleSelectField, AppendModelFieldList, AutoDeleteModelField
from util.qrcode import validate_qr_code
from . import models
from user.models import User


def duplicate_reference_validator(form, field):
    items = []
    for item in field.data:
        key = (item['reference_type'], item['value'])
        if key in items:
            raise ValidationError("Duplicate reference: %s: %s" % (
                models.ReferenceType(item['reference_type']).name.capitalize(),
                item['value'],
            ))
        items.append(key)


def reference_validator(form, field):
    if field.errors:
        return

    for data in field.data:
        if models.ReferenceType(data['reference_type']) == models.ReferenceType.QRCODE:
            # Check QR code is formatted sanely
            if not validate_qr_code(data['value']):
                raise ValidationError("QR Code is not a valid code")


class IssueReferenceForm(ModelForm):
    class Meta:
        model = models.IssueReference
        only = ['id', 'reference_type', 'value']

    id = wtforms.IntegerField(validators=[wtforms.validators.Optional()])


class IssueCommentForm(ModelForm):
    class Meta:
        model = models.IssueComment
        only = ['message']

    message = wtforms.TextAreaField(
        'Message',
        validators=[
            wtforms.validators.DataRequired()
        ]
    )


class IssueForm(ModelForm):

    class Meta:
        model = models.Issue

    title = wtforms.StringField(
        'Title',
        validators=[
            wtforms.validators.DataRequired(),
            wtforms.validators.Length(max=200),
        ]
    )

    category = QuerySelectField(
        "Category",
        query_factory=lambda: models.Category.query.order_by(models.Category.name),
        get_label=lambda x: x.name,
        allow_blank=False
    )

    severity = QuerySelectField(
        "Severity",
        query_factory=lambda: models.Severity.get_ordered(),
        get_label=lambda x: x.name,
        allow_blank=True
    )

    status = QuerySelectField(
        "Status",
        query_factory=lambda: models.Status.get_ordered(),
        get_label=lambda x: x.name,
        allow_blank=False
    )

    assigned_to = QuerySelectField(
        "Assigned To",
        query_factory=lambda: User.query.order_by(User.name),
        get_label=lambda x: x.name,
        allow_blank=True
    )

    tags = AutoCreateQueryMultipleSelectField(
        "Tags",
        query_factory=lambda: Tag.query.order_by(Tag.name),
        get_label=lambda x: x.name,
        create_factory=lambda text: Tag(name=text.strip().lower()),
        validators=[wtforms.validators.Optional()]
    )

    contact_email = wtforms.StringField('Contact Email', validators=[
        wtforms.validators.Optional(), wtforms.validators.Email()
    ])
    contact_phone = wtforms.StringField('Contact Phone', validators=[wtforms.validators.Length(max=20)])

    references = AutoDeleteModelField(
        wtforms.FormField(IssueReferenceForm),
        validators=[
            duplicate_reference_validator,
            reference_validator
        ]
    )

    comments = AppendModelFieldList(wtforms.FormField(IssueCommentForm))

    @property
    def flattened_errors(self):
        errors = {}
        for field, data in self.errors.items():
            field_errors = []
            for err in data:
                if isinstance(err, dict):
                    for k, v in err.items():
                        [field_errors.append(f'{k}: {_v}') for _v in v]
                else:
                    field_errors.append(err)
            errors[field] = field_errors
        return errors
