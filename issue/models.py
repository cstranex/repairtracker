import enum
from datetime import datetime

from sqlalchemy import event
from sqlalchemy_utils import EmailType, ChoiceType
from sqlalchemy_utils.models import Timestamp

from auth.authentication import current_user
from model import db
from tags.models import Category, Tag
from user.models import User
from util.colours import COLOURS


def get_current_user_id():
    if current_user:
        return current_user.id
    else:
        return None


class Status(db.Model):
    """
        The status of a particular issue, eg: NEW --> OPEN --> TESTING --> COMPLETED --> RETURNED

        The priority shows where in the list the status item ends up (Sort order)
        closes marks the issue as completed if true and the status is selected.

    """
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)
    priority = db.Column(db.Integer(), unique=True)
    closes = db.Column(db.Boolean())
    colour = db.Column(ChoiceType(COLOURS))

    @property
    def colour_name(self):
        if not self.colour:
            return 'grey'
        else:
            return self.colour.code

    @classmethod
    def get_ordered(cls):
        """Return the statuses in priority order"""
        return db.session.query(cls).order_by(Status.priority)

    @classmethod
    def get_closed_ids(cls):
        return [x[0] for x in db.session.query(Status.id).filter(Status.closes == True)]

    def __str__(self):
        return self.name


class Severity(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)
    priority = db.Column(db.Integer(), unique=True)  # between 0 and 100

    @classmethod
    def get_ordered(cls):
        """Return the severity in priority order"""
        return db.session.query(cls).order_by(Severity.priority)

    def __str__(self):
        return self.name


class Issue(db.Model, Timestamp):
    """A new equipment repair issue"""
    id = db.Column(db.Integer(), primary_key=True)

    title = db.Column(db.String(200))  # Nice description of issue

    reported_by_id = db.Column(
        db.Integer(),
        db.ForeignKey(User.id, ondelete='SET NULL'),
        default=get_current_user_id
    )  # Who entered it
    assigned_to_id = db.Column(db.Integer(), db.ForeignKey(User.id, ondelete='SET NULL'))  # Who is assigned to it
    category_id = db.Column(db.Integer(), db.ForeignKey(Category.id))
    severity_id = db.Column(db.Integer(), db.ForeignKey(Severity.id))
    status_id = db.Column(db.Integer(), db.ForeignKey(Status.id))  # Current status

    description = db.Column(db.Text())  # A longer description of the issue

    contact_name = db.Column(db.String(48))
    contact_email = db.Column(EmailType())
    contact_phone = db.Column(db.String(20))
    contact_company = db.Column(db.String(48))

    tags = db.relationship(Tag, secondary='issue_tags', backref='issues')

    reported_by = db.relationship(User, foreign_keys=[reported_by_id], backref='reported_issues')
    assigned_to = db.relationship(User, foreign_keys=[assigned_to_id], backref='assigned_issues')
    category = db.relationship(Category, backref='issues')
    severity = db.relationship(Severity)
    status = db.relationship(Status)

    references = db.relationship('IssueReference', backref='issue')
    comments = db.relationship('IssueComment', order_by=lambda: IssueComment.created.desc())
    logs = db.relationship('IssueLog', backref='issue', order_by=lambda: IssueLog.created.desc())

    @property
    def formatted_id(self):
        return f'{self.id:04}'

    @property
    def formatted_title(self):
        if len(self.title) > 20:
            trail = self.title[:20] + '...'
        else:
            trail = self.title

        return f'#{self.formatted_id}: {trail}'

    @property
    def age(self):
        return (datetime.utcnow() - self.created).days

    @property
    def is_open(self):
        return not self.status.closes

    @property
    def has_contact_information(self):
        return self.contact_name or self.contact_email or self.contact_phone or self.contact_company

    @property
    def history(self):
        """Return comments and logs"""
        data = []
        data.extend(self.logs)
        data.extend(self.comments)

        data.sort(key=lambda x: (x.created, isinstance(x, IssueComment)), reverse=True)

        for item in data:
            if isinstance(item, IssueLog):
                yield dict(log=item)
            elif isinstance(item, IssueComment):
                yield dict(comment=item)
        return data

    @property
    def referenced(self):
        references = db.session.query(IssueReference).filter(
            IssueReference.reference_type == ReferenceType.ISSUE,
            IssueReference.value == str(self.id)
        ).order_by(IssueReference.created.desc())
        return [reference.issue for reference in references]

    @classmethod
    def get_unassigned(cls, show_closed=True):
        """Return a query result of issues that have no assigned user"""
        query = db.session.query(cls).filter(Issue.assigned_to == None)
        if not show_closed:
            query = query.filter(~Issue.status_id.in_(Status.get_closed_ids()))
        return query

    @classmethod
    def get_assigned(cls, user: User, show_closed=True):
        query = db.session.query(cls).filter(Issue.assigned_to == user)
        if not show_closed:
            query = query.filter(~Issue.status_id.in_(Status.get_closed_ids()))
        return query

    @classmethod
    def get_reported(cls, user: User, show_closed=True):
        query = db.session.query(cls).filter(Issue.reported_by == user)
        if not show_closed:
            query = query.filter(~Issue.status_id.in_(Status.get_closed_ids()))
        return query

    @classmethod
    def get_last_modified(cls, show_closed=True):
        query = db.session.query(cls).order_by(Issue.updated.desc())
        if not show_closed:
            query = query.filter(~Issue.status_id.in_(Status.get_closed_ids()))
        return query

    @classmethod
    def get_open(cls):
        return db.session.query(cls).filter(~Issue.status_id.in_(Status.get_closed_ids()))

    @classmethod
    def get_by_category(cls, category):
        return db.session.query(cls).filter(Issue.category == category)


class IssueComment(db.Model, Timestamp):
    """Comments added by users to a particular issue"""
    id = db.Column(db.Integer(), primary_key=True)
    issue_id = db.Column(db.Integer(), db.ForeignKey(Issue.id, ondelete='CASCADE'))
    user_id = db.Column(
        db.Integer(),
        db.ForeignKey(User.id, ondelete='SET NULL'),
        default=get_current_user_id
    )  # Who is assigned to it
    message = db.Column(db.Text(), nullable=False)
    user = db.relationship(User)
    issue = db.relationship(Issue)


class ReferenceType(enum.IntEnum):
    """References that can be added"""
    ISSUE = 0  # Reference to a previous issue
    INVOICE = 1  # Reference to an invoice
    ORDER = 2  # Reference to an order
    QRCODE = 3  # Reference to a qrcode
    SERIAL = 4  # Reference to a serial number


class IssueReference(db.Model):
    """A reference to a particular item"""
    id = db.Column(db.Integer(), primary_key=True)
    issue_id = db.Column(db.Integer(), db.ForeignKey(Issue.id, ondelete='CASCADE'), nullable=False)

    reference_type: ReferenceType = db.Column(ChoiceType(ReferenceType, impl=db.Integer()), nullable=False)
    value = db.Column(db.String(50), nullable=False)

    created = db.Column(db.DateTime(), default=datetime.utcnow)

    @property
    def type_title(self):
        return ReferenceType(self.reference_type).name.capitalize()

    def __str__(self):
        return f"{self.type_title} {self.value}"


class LogActionType(enum.IntEnum):
    ADDED = 0
    CHANGED = 1
    REMOVED = 2


class IssueLog(db.Model):
    """A log of all actions to the issue"""
    @property
    def actions(self):
        return LogActionType

    id = db.Column(db.Integer(), primary_key=True)
    created = db.Column(db.DateTime(), default=datetime.utcnow)

    issue_id = db.Column(db.Integer(), db.ForeignKey(Issue.id, ondelete='CASCADE'))

    user_id = db.Column(
        db.Integer(),
        db.ForeignKey(User.id, ondelete='SET NULL'),
        default=get_current_user_id
    )  # Who is assigned to it

    new_value = db.Column(db.Text())
    old_value = db.Column(db.Text())
    property = db.Column(db.Text(), nullable=False)
    action = db.Column(ChoiceType(LogActionType, impl=db.Integer()), nullable=False)
    user = db.relationship(User)


issue_tags = db.Table(
    'issue_tags',
    db.Column('issue_id', db.Integer(), db.ForeignKey(Issue.id)),
    db.Column('tag_id', db.Integer(), db.ForeignKey(Tag.id))
)


@event.listens_for(Issue, 'before_update')
def before_update(mapper, connection, target):
    state = db.inspect(target)
    changes = {}

    for attr in state.attrs:
        if attr.key in ['updated', 'created', 'comments'] or attr.key.endswith('_id'):
            continue
        hist = attr.load_history()

        if not hist.has_changes():
            continue

        if attr.key.startswith('contact_'):
            changes['contact information'] = (' ', ' ')
        else:
            changes[attr.key] = (hist.deleted, hist.added)

    if changes:
        for key, values in changes.items():
            old, new = values
            issue_log = IssueLog()
            issue_log.user = current_user
            issue_log.issue = target
            issue_log.property = key
            if new:
                issue_log.new_value = ', '.join([str(x) for x in new])

            if old:
                issue_log.old_value = ', '.join([str(x) for x in old])

            if not new:
                issue_log.action = LogActionType.REMOVED
            elif old:
                issue_log.action = LogActionType.CHANGED
            else:
                issue_log.action = LogActionType.ADDED

            db.session.add(issue_log)
