from flask import Blueprint, jsonify, request

from auth.authentication import login_required
from .search import search_issues

app = Blueprint('issue_api', __name__, url_prefix='/api/1/issue')


@app.before_request
@login_required
def before_request():
    """Ensure all endpoints are authenticated"""


@app.route('/search')
def search():
    """Query the issues"""

    results = search_issues(request.args.get('query'))
    return jsonify({
        'results': list(results)
    })

