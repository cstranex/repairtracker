from flask import Blueprint, render_template

from auth.authentication import login_required
from tags.models import Category

app = Blueprint('category', __name__, url_prefix='/category')


@app.before_request
@login_required
def before_request():
    """Ensure all endpoints are authenticated"""


@app.route('/')
def overview():
    """View all categories"""
    return render_template('category/overview.html', categories=Category.query.order_by(Category.name))


@app.route('/<int:category>')
def view(category: int):
    """View a specific category"""
    category = Category.query.get_or_404(category)

    return render_template('category/issues.html', category=category, issues=category.issues)
