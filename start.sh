#!/usr/bin/env bash
export FLASK_APP=main:app
source .venv/bin/activate
gunicorn --reload -c /etc/gunicorn.conf $FLASK_APP
