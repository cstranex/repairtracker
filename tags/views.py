from flask import Blueprint, redirect, url_for, render_template

from auth.authentication import login_required
from tags.models import Tag

app = Blueprint('tag', __name__, url_prefix='/tag')


@app.before_request
@login_required
def before_request():
    """Ensure all endpoints are authenticated"""


@app.route('/')
def overview():
    return render_template('tags/overview.html', tags=Tag.query)


@app.route('/<int:tag>')
def view(tag: int):
    """View a specific tag"""
    return 'Not Implemented'


@app.route('/name/<string:name>')
def view_name(name: str):
    tag = Tag.query.filter(Tag.name.ilike(name)).first_or_404()
    return redirect(url_for('tag.view', tag=tag.id))
