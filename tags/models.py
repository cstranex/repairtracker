from sqlalchemy_utils import ChoiceType

from model import db
from util.colours import COLOURS


class Tag(db.Model):
    """Multiple tags per issue. Examples: sound, microphone"""

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100), unique=True)  # Must be lowercase
    colour = db.Column(ChoiceType(COLOURS))

    def __str__(self):
        return self.name


class Category(db.Model):
    """One category per issue. Examples: external-repairs"""
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100), unique=True)  # Must be lowercase

    def __str__(self):
        return self.name
