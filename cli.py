from flask.cli import with_appcontext, cli
import click

from model import db


@cli.command('init')
@with_appcontext
def init_db():
    """Initialise the database on first run. Automatically retries if the database is not ready until a 2 minute
    timeout"""
    from issue import models
    from tags import models
    from user import models

    db.create_all()


@cli.command('create-severity')
@click.argument('name')
@click.argument('priority')
@with_appcontext
def create_severity(name, priority):
    """Create a new severity"""
    from issue import models
    db.session.add(models.Severity(name=name, priority=int(priority)))
    db.session.commit()


@cli.command('create-category')
@click.argument('name')
@with_appcontext
def create_category(name):
    """Create a new category"""
    from tags import models

    db.session.add(models.Category(name=name))
    db.session.commit()


@cli.command('create-tag')
@click.argument('name')
@click.argument('colour')
@with_appcontext
def create_category(name, colour):
    """Create a new tag"""
    from tags import models

    selected_colour = None
    for n, col in enumerate(models.COLOURS):
        if col[0] == colour:
            selected_colour = col
            break

    db.session.add(models.Tag(name=name, colour=selected_colour))
    db.session.commit()
