FROM python:3.7 AS base
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 PYTHONUNBUFFERED=1


RUN apt-get update && \
    apt-get install -y default-libmysqlclient-dev --no-install-recommends && \
    rm -rf /var/lib/apt/lists/*

RUN groupadd -g 999 www && useradd -r -u 999 -g www www

# Install requirements
FROM base AS builder
RUN apt-get update && \
    apt-get install -y build-essential --no-install-recommends && \
    rm -rf /var/lib/apt/lists/* && \
    pip install pipenv && \
    mkdir /app
ADD ./Pipfile /app
ADD ./Pipfile.lock /app
WORKDIR /app
RUN PIPENV_VENV_IN_PROJECT="y" pipenv install --deploy && \
    apt-get remove -y build-essential && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/* && \
    rm /root/.cache -rf

# Copy the dependencies and the app and start gunicorn
FROM base AS release
ADD . /app
ADD ./start.sh /app
COPY --from=builder /app/.venv /app/.venv
RUN echo "export FLASK_APP=main:app" >> /etc/bash.bashrc && echo "source .venv/bin/activate" >> /etc/bash.bashrc
USER www
WORKDIR /app
CMD [ "./start.sh" ]
