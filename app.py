import pkgutil
import importlib
from flask import Flask, request
from flask_babelex import Babel
from flask_wtf import CSRFProtect

from auth import authentication
from auth.authentication import current_user
from issue.models import ReferenceType
from model import db
from config import get_configuration
import cli
from util.date_format import format_datetime
from util.markdown import markdown
from util.render_bug import referenceissue, renderissue


def register_blueprints(current_app):
    # Import all blueprint views
    for module in pkgutil.iter_modules(['.']):
        if module.ispkg:
            try:
                view = importlib.import_module(f'{module.name}.views')
            except ImportError as e:
                if e.name != f'{module.name}.views':
                    raise
                else:
                    continue
            current_app.register_blueprint(view.app)


def register_apis(current_app):
    # Import all blueprint views
    for module in pkgutil.iter_modules(['.']):
        if module.ispkg:
            try:
                view = importlib.import_module(f'{module.name}.api')
            except ImportError as e:
                if e.name != f'{module.name}.api':
                    raise
                else:
                    continue
            current_app.register_blueprint(view.app)


def create_app():
    app = Flask(__name__)
    app.cli.add_command(cli.init_db)
    app.cli.add_command(cli.create_category)
    app.cli.add_command(cli.create_severity)

    # Get configuration
    app.config.from_object(get_configuration())
    app.secret_key = app.config['SECRET_KEY']

    db.init_app(app)

    register_blueprints(app)
    register_apis(app)

    authentication.create_blueprint(app)

    CSRFProtect(app)

    babel = Babel(app)

    app.add_template_filter(format_datetime, 'datetime')
    app.add_template_filter(markdown)
    app.add_template_filter(renderissue)
    app.add_template_filter(referenceissue)

    @babel.localeselector
    def get_locale():
        return request.accept_languages.best_match(['en'])

    @babel.timezoneselector
    def get_timezone():
        if current_user and current_user.timezone:
            return current_user.timezone
        else:
            return app.config.get('DEFAULT_TIMEZONE', 'UTC')

    @app.context_processor
    def ctx():
        return {
            'current_user': authentication.current_user,
            'ReferenceType': ReferenceType,
        }

    return app


if __name__ == '__main__':
    create_app().run()
