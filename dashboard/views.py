from flask import Blueprint, render_template

from auth.authentication import login_required, current_user
from issue.models import Issue

app = Blueprint('dashboard', __name__, url_prefix='/')


@app.before_request
@login_required
def before_request():
    """Ensure all endpoints are authenticated"""


@app.route('/')
def index():
    return render_template(
        'index.html',
        unassigned_issues=Issue.get_unassigned(show_closed=False).limit(10),
        assigned_issues=Issue.get_assigned(current_user, show_closed=False).limit(10),
        reported_issues=Issue.get_reported(current_user, show_closed=False).limit(10),
        oldest_opened=Issue.get_open().order_by(Issue.created).limit(10),
    )
