from sqlalchemy_utils import EmailType
from model import db
from util.colours import COLOURS


class User(db.Model):
    """Represents a user class"""
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(EmailType())

    timezone = db.Column(db.String(32), default='UTC')

    disabled = db.Column(db.Boolean())  # Prevent login
    admin = db.Column(db.Boolean())  # If the user is an admin

    @property
    def colour(self):
        return COLOURS[self.id % len(COLOURS)][0]

    @classmethod
    def get_user_by_email(cls, email):
        return db.session.query(cls).filter(cls.email == email).first()

    @classmethod
    def create_user(cls, email, name):
        """Create a new user account"""
        u = cls()
        u.email = email
        u.name = name
        db.session.add(u)
        db.session.commit()

    def __str__(self):
        return self.name
