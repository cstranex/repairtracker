from wtforms import StringField
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, Length


class EditUserForm(FlaskForm):
    name = StringField(label='Name', validators=[
        DataRequired(),
        Length(min=1, max=100)
    ])
