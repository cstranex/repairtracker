from flask import Blueprint, render_template, flash

from auth.authentication import login_required, current_user
from issue.models import Issue
from model import db
from user.forms import EditUserForm
from user.models import User

app = Blueprint('user', __name__, url_prefix='/user')


@app.before_request
@login_required
def before_request():
    """Ensure all endpoints are authenticated"""


@app.route('/edit', methods=['GET', 'POST'])
def edit_user():
    form = EditUserForm(obj=current_user)

    if form.validate_on_submit():
        form.populate_obj(current_user)
        db.session.add(current_user)
        db.session.commit()
        flash("Updated Details", 'success')

    return render_template('user/edit.html', form=form)


@app.route('/<int:user>')
def view(user: int):
    user = User.query.get_or_404(user)

    return render_template('user/issues.html', user=user, issues=Issue.get_assigned(user, show_closed=True))
